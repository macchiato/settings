export class PageSettings extends HTMLElement {
  textEn = {
    general: 'General',
    template: 'Template',
    codeMirror: 'CodeMirror',
    outbound: 'Outbound connections',
    inbound: 'Inbound connections',
    netAccess: 'Direct network access (CSP)',
  }

  textEs = {
    general: 'General',
    template: 'Template',
    codeMirror: 'CodeMirror',
    outbound: 'Conexiones salientes',
    inbound: 'Conexiones entrantes',
    netAccess: 'Acceso directo a la red (CSP)',
  }

  constructor() {
    super()
    this.attachShadow({mode: 'open'})
    this.language = navigator.language

    const generalHeading = document.createElement('div')
    generalHeading.classList.add('heading')
    generalHeading.innerText = this.text.general

    const templateLabel = document.createElement('label')
    templateLabel.innerText = this.text.template
    this.templateEl = document.createElement('input')
    this.templateEl.type = 'checkbox'
    templateLabel.prepend(this.templateEl)

    const codeMirrorLabel = document.createElement('label')
    codeMirrorLabel.innerText = this.text.codeMirror
    this.codeMirrorEl = document.createElement('input')
    this.codeMirrorEl.type = 'checkbox'
    codeMirrorLabel.prepend(this.codeMirrorEl)

    const generalDiv = document.createElement('div')
    generalDiv.append(templateLabel, codeMirrorLabel)

    const outboundHeading = document.createElement('div')
    outboundHeading.classList.add('heading')
    outboundHeading.innerText = this.text.outbound
    this.outbound = document.createElement(
      'm-settings-connections'
    )
    this.outbound.type = 'outbound'
    const inboundHeading = document.createElement('div')
    inboundHeading.classList.add('heading')
    inboundHeading.innerText = this.text.inbound
    this.inbound = document.createElement(
      'm-settings-connections'
    )
    this.inbound.type = 'inbound'
    const netSelectHeading = document.createElement('div')
    netSelectHeading.classList.add('heading')
    netSelectHeading.innerText = this.text.netAccess
    this.network = document.createElement(
      'm-settings-network-settings'
    )
    this.shadowRoot.append(
      generalHeading,
      generalDiv,
      outboundHeading,
      this.outbound,
      inboundHeading,
      this.inbound,
      netSelectHeading,
      this.network,
    )
  }

  connectedCallback() {
    const style = document.createElement('style')
    style.textContent = `
      :host {
        max-height: 55vh;
        display: flex;
        flex-direction: column;
        align-items: stretch;
        overflow-y: auto;
      }
      * {
        padding-left: 10px;
      }
      div.heading {
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
        background: #f2dbd8;
        padding: 3px;
        margin-bottom: 10px;
        margin-top: 10px;
        font-weight: bold;
      }
    `
    this.shadowRoot.append(style)
  }

  get data() {
    return {
      ...this.network.data,
      connections: {
        outbound: this.outbound.data,
        inbound: this.inbound.data,
      },
      template: this.templateEl.checked,
      codeMirror: this.codeMirrorEl.checked,
    }
  }

  set data(value) {
    this.templateEl.checked = !!value.template
    this.codeMirrorEl.checked = !!value.codeMirror
    this.outbound.data = value.connections?.outbound || {}
    this.inbound.data = value.connections?.inbound || {}
    this.network.data = {
      networkAccess: value.networkAccess
    }
  }

  set path(value) {
    this._path = value
    this.outbound.path = value
    this.inbound.path = value
  }

  get path() {
    return this._path
  }

  set cspProfiles(value) {
    this.network.cspProfiles = value
  }

  set checkExists(value) {
    this._checkExists = value
    this.outbound.checkExists = value
    this.inbound.checkExists = value
  }

  get checkExists() {
    return this._checkExists
  }

  get language() {
    return this._language
  }

  set language(language) {
    this._language = language
    this.text = this.langEs ? this.textEs : this.textEn
  }

  get langEs() {
    return /^es\b/.test(this.language)
  }

  get lang() {
    return this.language.split('-')[0]
  }
}