export class Connections extends HTMLElement {
  icons = {
    del: `
      <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
  <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
</svg>
    `
  }

  textEn = {
    add: 'Add connection',
    cancel: 'Cancel',
    read: 'read',
    readWrite: 'read and write',
  }

  textEs = {
    add: 'Añadir conexión',
    cancel: 'Cancelar',
    read: 'leer',
    readWrite: 'leer y escribir',
  }

  constructor() {
    super()
    this.attachShadow({mode: 'open'})
    this.language = navigator.language
    this.content = document.createElement('div')
    const bGroup = document.createElement(
      'm-forms-button-group'
    )
    bGroup.addPrimary(this.text.add, () => {
      this.add()
    })
    this.shadowRoot.append(
      this.content,
      bGroup,
    )
  }

  connectedCallback() {
    const style = document.createElement('style')
    style.textContent = `
      :host {
        display: flex;
        flex-direction: column;
        align-items: stretch;
        margin-bottom: 5px;
      }
      m-dialog::part(footer) {
        padding-top: 15px;
      }
      button.icon {
        border: none;
        background: inherit;
        color: #555;
      }
      button.icon svg {
        width: 12px;
        height: 12px;
      }
      .connection {
        display: flex;
        flex-direction: row;
        gap: 8px;
        align-items: center;
      }
      .access {
        font-size: 80%;
        background: #ccc;
        border-radius: 3px;
        padding: 5px;
      }
    `
    this.shadowRoot.append(style)
  }

  add() {
    const dialog = document.createElement('m-dialog')
    dialog.top = 180
    const edit = document.createElement(
      'm-settings-connection-edit'
    )
    dialog.bodyEl.append(edit)
    const bGroup = document.createElement(
      'm-forms-button-group'
    )
    bGroup.addPrimary(this.text.add, () => {
      const path = edit.pageInput.value
      const access = edit.accessSelect.value
      const exists = this.checkExists(path)
      if (!exists) {
        edit.error = 'doesntExist'
        return
      } else if (path === this.path) {
        edit.error = 'samePage'
        return
      } else if (this.data[path] ?? true !== true) {
        edit.error = 'alreadyConnected'
        return
      }
      this.data = {
        ...this.data,
        [path]: access,
      }
      dialog.close()
    })
    bGroup.addCancel(this.text.cancel, () => {
      dialog.close()
    })
    dialog.footerEl.appendChild(bGroup)
    this.shadowRoot.append(dialog)
    dialog.open()
  }

  display() {
    const entries = Object.entries(this.data).filter(
      ([k, v]) => v !== undefined
    )
    const content = entries.map(([path, access]) => {
      const el = document.createElement('div')
      el.classList.add('connection')
      const pathEl = document.createElement('span')
      pathEl.innerText = path
      el.append(pathEl)
      const accessEl = document.createElement('span')
      accessEl.innerText = this.text[access]
      accessEl.classList.add('access')
      const delBtn = document.createElement('button')
      delBtn.classList.add('delete', 'icon')
      delBtn.innerHTML = this.icons.del
      delBtn.addEventListener('click', () => {
        this.data = {...this.data, [path]: undefined}
      })
      el.append(accessEl, delBtn)
      return el
    })
    this.content.replaceChildren(...content)
  }

  set type(value) {
    this._type = value
  }

  get type() {
    return this._type
  }

  get data() {
    return this._data
  }

  set data(value) {
    this._data = value
    this.display()
  }

  get language() {
    return this._language
  }

  set language(language) {
    this._language = language
    this.text = this.langEs ? this.textEs : this.textEn
  }

  get langEs() {
    return /^es\b/.test(this.language)
  }

  get lang() {
    return this.language.split('-')[0]
  }
}