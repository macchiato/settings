export class NetworkSettings extends HTMLElement {
  textEn = {
    csp: 'Content Security Policy',
  }

  textEs = {
    csp: 'Política de Seguridad de Contenido',
  }

  accessOptions = {
    local: {
      option: {
        en: "network access off",
        es: "acceso a la red desactivado",
      },
      text: {
        en: 'direct network access off',
        es: 'acceso directo a la red desactivado',
      },
      details: {
        en: "Direct network access is off and the page can't send data. This prevents data from being exposed even if the code of the page isn't trusted. It is a good option if this page will contain private data.",
        es: "El acceso directo a la red está desactivado y la página no puede enviar datos. Esto evita que los datos queden expuestos incluso si el código de la página no es de confianza. Es una buena opción si esta página contendrá datos privados.",
      },
    },
    unpkg: {
      option: {
        en: 'UNPKG',
        es: 'UNPKG',
      },
      text: {
        en: 'access to UNPKG only',
        es: 'acceso a UNPKG solamente',
      },
      details: {
        en: "The page can make requests to UNPKG. This could allow data from your page to be sent to the servers if the code in the page sends it. This means that if you have private data in the page, you should either trust the code on the page not to send it to these servers, or you should trust these servers, or both.",
        es: "La página puede realizar solicitudes a UNPKG. Esto podría permitir que los datos de su página se envíen a los servidores si el código de la página los envía. Esto significa que si tiene datos privados en la página, debe confiar en el código de la página para no enviarlos a estos servidores, o debe confiar en estos servidores, o en ambos.",
      },
    },
    open: {
      option: {
        en: 'network open',
        es: 'red abierta',
      },
      text: {
        en: 'network open (direct access to any site)',
        es: 'red abierta (acceso directo a cualquier sitio)',
      },
      details: {
        en: "Network access is open, so the page can send or receive requests to any site. You should either A) have no private data in the page, or B) trust the code completely. This makes the page similar to major code playgrounds like codesandbox.com, stackblitz.com, jsbin.com, and codepen.io.",
        es: "El acceso a la red está abierto, por lo que la página puede enviar o recibir solicitudes a cualquier sitio. Debería A) no tener datos privados en la página, o B) confiar completamente en el código. Esto hace que la página sea similar a los principales juegos de código como codesandbox.com, stackblitz.com, jsbin.com y codepen.io.",
      },
    },
  }

  constructor() {
    super()
    this.attachShadow({mode: 'open'})
    this.language = navigator.language
    const netSelectField = document.createElement('div')
    netSelectField.classList.add('field')
    this.netSelect = document.createElement('select')
    this.netSelect.addEventListener(
      'change', () => this.display()
    )
    netSelectField.append(this.netSelect)
    const netOptions = Object.entries(
      this.accessOptions
    ).map(([value, {option}]) => {
      const el = document.createElement('option')
      el.value = value
      el.innerText = option[this.lang]
      return el
    })
    this.netSelect.append(...netOptions)
    this.netHeading = document.createElement('h3')
    this.netText = document.createElement('p')
    this.cspLabel = document.createElement(
      'div'
    )
    this.cspLabel.classList.add('csp-label')
    this.cspLabel.innerText = (
      this.text.csp + ' (CSP):'
    )
    this.netCsp = document.createElement('div')
    this.netCsp.classList.add('csp')
    this.shadowRoot.append(
      netSelectField,
      this.netHeading,
      this.netText,
      this.cspLabel,
      this.netCsp,
    )
  }

  connectedCallback() {
    const style = document.createElement('style')
    style.textContent = `
      :host {
        display: flex;
        flex-direction: column;
        align-items: stretch;
      }
      div.field {
        display: flex;
        flex-direction: row;
      }
      h1, h2, h3, p {
        margin: 3px 0;
      }
      .csp-label {
        font-weight: bold;
      }
      .csp {
        font-family: monospace;
      }
    `
    this.shadowRoot.append(style)
  }

  display() {
    const value = this.netSelect.value
    const opt = this.accessOptions[value]
    const l = this.lang
    this.netHeading.innerText = opt.text[l]
    this.netText.innerText = opt.details[l]
    this.netCsp.innerText = (
      this.cspProfiles[value]
    )
  }

  get data() {
    return {
      networkAccess: this.netSelect.value,
    }
  }

  set data(value) {
    this.netText.innerText = JSON.stringify(value)
    this.netSelect.value = value.networkAccess ?? 'local'
    this.display()
  }

  get language() {
    return this._language
  }

  set language(language) {
    this._language = language
    this.text = this.langEs ? this.textEs : this.textEn
  }

  get langEs() {
    return /^es\b/.test(this.language)
  }

  get lang() {
    return this.language.split('-')[0]
  }
}