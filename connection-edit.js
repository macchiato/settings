export class ConnectionEdit extends HTMLElement {
  textEn = {
    page: 'Page',
    access: 'Access',
    read: 'read',
    readWrite: 'read and write',
    doesntExist: "Error: The page doesn't exist",
    samePage: "Error: Cannot connect to same page",
    alreadyConnected: "Error: This page is already connected",
  }

  textEs = {
    page: 'Página',
    access: 'Accesso',
    read: 'leer',
    readWrite: 'leer y escribir',
    doesntExist: 'Error: La página no existe',
    samePage: "Error: no se puede conectar a la misma página",
    alreadyConnected: "Error: esta página ya está conectada",
  }

  constructor() {
    super()
    this.attachShadow({mode: 'open'})
    this.language = navigator.language
    const pageLabel = document.createElement('label')
    pageLabel.innerText = this.text.page
    this.pageInput = document.createElement('input')
    const accessLabel = document.createElement('label')
    accessLabel.innerText = this.text.access
    this.accessSelect = document.createElement('select')
    const wrap = document.createElement('div')
    wrap.append(this.accessSelect)
    const opts = ['read', 'readWrite'].map(value => {
      const el = document.createElement('option')
      el.value = value
      el.innerText = this.text[value]
      return el
    })
    this.accessSelect.append(...opts)
    this.accessSelect.value = 'read'
    const fields = document.createElement('div')
    fields.classList.add('fields')
    fields.append(
      pageLabel,
      this.pageInput,
      accessLabel,
      wrap,
    )
    this.shadowRoot.append(
      fields,
    )
  }

  connectedCallback() {
    const style = document.createElement('style')
    style.textContent = `
      :host {
        display: flex;
        flex-direction: column;
        align-items: stretch;
      }
      .fields {
        display: grid;
        grid-template-columns: fit-content(50%) 1fr;
        gap: 5px 10px;
      }
      .error {
        color: red;
      }
    `
    this.shadowRoot.append(style)
  }

  get data() {
    return {
      networkAccess: this.netSelect.value,
    }
  }

  set data(value) {
    this.netText.innerText = JSON.stringify(value)
    this.netSelect.value = value.networkAccess ?? 'local'
  }

  set error(error) {
    this._error = error
    if (error === undefined) {
      if (this.errorEl) {
        this.errorEl.remove()
        this.errorEl = undefined
      }
    } else {
      if (!this.errorEl) {
        this.errorEl = document.createElement('p')
        this.errorEl.classList.add('error')
        this.shadowRoot.append(this.errorEl)
      }
      this.errorEl.innerText = this.text[error]
    }
  }

  get error() {
    return this._error
  }

  get language() {
    return this._language
  }

  set language(language) {
    this._language = language
    this.text = this.langEs ? this.textEs : this.textEn
  }

  get langEs() {
    return /^es\b/.test(this.language)
  }

  get lang() {
    return this.language.split('-')[0]
  }
}